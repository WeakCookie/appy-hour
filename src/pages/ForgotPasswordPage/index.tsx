import PublicLayout from 'components/PublicLayout'
import ForgotPasswordPage from '../../components/Pages/ForgotPasswordPage'
import styles from './forgotPasswordPage.module.scss'

const ForgotPasswordLayout = () => {
  return (
    <PublicLayout>
      <div className={styles.container}>
        <ForgotPasswordPage title="Reset Password" />
      </div>
    </PublicLayout>
  )
}

export default ForgotPasswordLayout
