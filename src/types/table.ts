import { ReactNode } from 'react'

export declare interface ITableHeader {
  Header: string | ReactNode
  accessor?: string
  Cell?: ReactNode
  columns?: ITableHeader[]
  SubCell?: any
  canExpand?: boolean
}
