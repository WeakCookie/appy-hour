export interface ICredential {
  _id?: string
  username_verified?: boolean
  username: string
  password: string
}

export interface IToken {
  access_token: string
  id_token: string
}
