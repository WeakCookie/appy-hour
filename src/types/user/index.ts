import { RoleType } from 'constants/enums/role'

export declare interface ILoginRequest {
  username: string
  password: string
  type?: RoleType
}
