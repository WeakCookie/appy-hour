export type TResData<T> = {
  data: T
  status: number
  isSuccess: boolean
}
