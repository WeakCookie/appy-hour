export enum RoleType {
  ESTABLISHMENT = 'establishment',
  SUPPLIER = 'supplier',
}
