import { IOption } from "components/InputGroup/InputDropdown";
import { WeekDayEnum } from "components/Pages/SpecialPage/components/SpecialTabs/components/CreateWeeklySpecialModal/constants";

import {
  CategoryEnum,
  CategoryType,
  DayOfWeek,
  DealTypeEnum,
  PriceType,
} from "./enums/special";

export interface IChartDataItem {
  chartData: IDateValue[];
  totalViews: number;
  changes: number;
}

export interface IListDataItem {
  day?: number;
  end?: Date;
  date?: string;
  event?: null;
  start?: Date;
  views?: {
    changes?: {
      lastWeek?: number;
      thisWeek?: number;
    };
    lastWeek?: number;
    thisWeek?: number;
    averageSincePublished?: number;
  };
  return?: true;
  details?: string;
  title?: string;
  refId?: number;
  deleted?: string;
  createdAt?: string;
  specialId?: number;
}

export interface IWeeklyImpressions {
  special?: IListDataItem[];
  event?: IListDataItem[];
}
export interface IAnalyticsData {
  special?: IChartDataItem;
  event?: IChartDataItem;
  profileView?: IChartDataItem;
  favorite?: IChartDataItem;
  weeklyImpressions?: IWeeklyImpressions;
}

export interface IDateValue {
  x: string;
  y: number;
}

export interface IDashboardData {
  specialData?: IDateValue[];
  specialDataChanges?: string;
  eventData?: IDateValue[];
  eventDataChanges?: string;
  profileViewData?: IDateValue[];
  profileViewDataChanges?: string;
  favoriteData?: IDateValue[];
  favoriteDataChanges?: string;
  weeklyImpressions?: IWeeklyImpressions;
}

export interface ISpecial {
  special_id?: number;
  price: number;
  price_type_ref_id: PriceType;
  details: string;
  deal_type_ref_id: DealTypeEnum;
  day: DayOfWeek;
  isCurrent?: boolean;
  start_time: Date;
  end_time: Date;
  dinein?: CategoryEnum;
  carryout?: CategoryEnum;
  created_at?: Date;
  updated_at?: Date;
  occurDays?: boolean[];
  isTodaySpecial?: boolean;
  days?: Date[];
  event_id?: number;
}

export interface ICreateSpecial {
  duration?: {
    hour: Date;
    minute: Date;
  };
  startType?: string;
  startTime?: Date;
  endTime?: Date;
  description: string;
  price?: string;
  percentage?: string;
  priceType: PriceType | string;
  dealType?: DealTypeEnum;
  type: CategoryType[];
  days?: WeekDayEnum[];
  startDate?: Date;
  previousDays?: WeekDayEnum[];
  editingSpecial?: ISpecial;
  editingSpecialId?: number | undefined;
}

export interface IBusinessInfo {
  profilePicture?: any;
  profilePictureUrl?: any;
  name?: string;
  phone_num?: string;
  image_name?: string;
  address_1?: string;
  address_2?: string;
  city?: string;
  state?: string;
  state_code_ref_id?: string;
  zip?: string;
  website?: string;
  resWebsite?: string;
  menu_link?: string;
  mon_open?: Date | "";
  mon_close?: Date | "";
  tue_open?: Date | "";
  tue_close?: Date | "";
  wed_open?: Date | "";
  wed_close?: Date | "";
  thu_open?: Date | "";
  thu_close?: Date | "";
  fri_open?: Date | "";
  fri_close?: Date | "";
  sat_open?: Date | "";
  sat_close?: Date | "";
  sun_open?: Date | "";
  sun_close?: Date | "";
  description?: string;
  delivery?: boolean;
  takeout?: boolean;
  patio?: boolean;
  rooftop?: boolean;
  brunch?: boolean;
  dog_friendly?: boolean;
}

export interface IAccountSetting {
  email: string;
  phone_num: string;
  username: string;
}

export interface IChangePassword {
  currentPassword: string;
  newPassword: string;
  newPasswordConfirm: string;
}

export interface IAddManager {
  establishment: IOption;
  email: string;
  phone_num?: string;
  username: string;
}

export interface IEventSpecial {
  special_id?: number;
  event_id?: number;
  special?: ISpecial;
}

export interface IEvent {
  event_id?: number;
  isCurrent?: boolean;
  title?: string;
  details?: string;
  price?: string;
  description?: string;
  eventSpecials?: IEventSpecial[];
  specials?: ISpecial[];
  day?: DayOfWeek;
  days?: WeekDayEnum[];
  previousDays?: WeekDayEnum[];
  start_time?: Date;
  end_time?: Date;
  startDate?: Date;
  endDate?: Date;
  created_at?: Date;
  updated_at?: Date;
}

export interface IEstablishment {
  establishment_id?: number;
  name?: string;
  description?: string;
  address_id?: number;
  hours_id?: number;
  is_active?: number;
  phone_num?: number;
  show_in_app?: number;
  website?: string;
  resWebsite?: string;
  menu_link?: string;
  facebook?: string;
  snapchat?: string;
  instagram?: string;
  twitter?: string;
  dog_friendly?: number;
  patio?: number;
  rooftop?: number;
  na_options?: number;
  brunch?: number;
  delivery?: number;
  takeout?: number;
  black_owned?: number;
  created_at?: Date;
  updated_at?: Date;
  establishmentEvents?: IEstablishmentEvent[];
}

export interface IEstablishmentEvent {
  establishment_event_id: number;
  event_id: number;
  establishment_id: number;
  event: IEvent;
}

export interface IPagination {
  limit: number;
  offset: number;
}
