import { useContext, createContext } from 'react'
import { rootStore } from '../stores'

export const StoreContext = createContext(rootStore)

export const useStore = () => {
  return useContext(StoreContext)
}
