const config = {
  apiBaseUrl: process.env.API_URL || '',
}

export default config
