import cx from 'classnames'
import { ListGroup as RSListGroup, ListGroupProps as RSListGroupProps } from 'reactstrap'

const ListGroup = (props: RSListGroupProps) => {
  const { className, ...otherProps } = props
  return <RSListGroup {...otherProps} className={cx(className)} />
}

export default ListGroup
