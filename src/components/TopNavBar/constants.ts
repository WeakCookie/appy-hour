import routes from 'routes'
import AuthStore, { AuthenticateParams } from 'stores/authStore'

export const profileMenus = [
  {
    label: 'My Account',
    icon: 'uil uil-user-circle',
    redirectTo: routes.myAccount.value,
  },
  {
    label: 'Switch establishment',
    icon: 'las la-exchange-alt',
    redirectTo: '',
  },
  {
    label: 'Logout',
    icon: 'las la-sign-out-alt',
    redirectTo: routes.home.value,
    handler: (authStore: AuthStore) => {
      authStore.setAccessToken('', undefined)
      localStorage.removeItem(AuthenticateParams.ACCESS_TOKEN)
      sessionStorage.removeItem(AuthenticateParams.ACCESS_TOKEN)
    },
  },
]
