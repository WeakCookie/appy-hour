import logoSm from 'Assets/images/app_icon.png'
import logo from 'Assets/images/app_icon_1024.png'
import classNames from 'classnames'
import { useStore } from 'hooks/useStore'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import ProfileDropdown from './components/ProfileDropdown'
import SearchDropdown from './components/SearchDropdown'
import { profileMenus } from './constants'
import styles from './topNavbar.module.scss'

type TopNavbarProps = {
  hideLogo?: boolean
  navCssClasses?: string
  toggleSidebarCollapse: () => void
}

const TopNavbar = (props: TopNavbarProps) => {
  const { businessInfoStore } = useStore()
  const { generalInfo } = businessInfoStore
  const hideLogo = props.hideLogo || false
  const navCssClasses = props.navCssClasses || ''
  const containerCssClasses = !hideLogo ? 'container-fluid' : ''

  return (
    <div className={`navbar-custom ${navCssClasses}`}>
      <div className={containerCssClasses}>
        {!hideLogo && (
          <Link to="/" className="topnav-logo">
            <span className="topnav-logo-lg">
              <img src={logo} alt="logo" height="16" />
            </span>
            <span className="topnav-logo-sm">
              <img src={logoSm} alt="logo" height="16" />
            </span>
          </Link>
        )}

        <ul className="list-unstyled topbar-right-menu float-right mb-0">
          <li className="notification-list topbar-dropdown d-lg-none">
            <SearchDropdown />
          </li>
          <li className="notification-list">
            <ProfileDropdown
              profilePic={generalInfo.profilePictureUrl}
              menuItems={profileMenus}
              username={generalInfo.name || ''}
            />
          </li>
        </ul>

        <div className={styles.leftSideWrapper}>
          <i className={classNames('las la-bars', styles.collapseIcon)} onClick={props.toggleSidebarCollapse}></i>
        </div>
      </div>
    </div>
  )
}

export default observer(TopNavbar)
