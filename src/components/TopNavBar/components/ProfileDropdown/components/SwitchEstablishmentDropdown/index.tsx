import { useEffect, useState } from 'react'
import classNames from 'classnames'
import cx from 'classnames'
import { useStore } from 'hooks/useStore'
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
import { getValidArray } from 'utils/common'
import styles from './switchEstablishmentDropdown.module.scss'

interface ISwitchEstablishmentDropdownProps {
  label: String
  icon: String
  className?: String
}

const SwitchEstablishmentDropdown = (props: ISwitchEstablishmentDropdownProps) => {
  const { label, icon, className } = props
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const { authStore, establishmentStore } = useStore()
  const { establishments = [] } = establishmentStore
  const establishmentOptions = getValidArray(establishments).map((establishment: any) => ({
    label: establishment.name,
    value: establishment.establishment_id,
  }))

  function handleChangeEst(id: number) {
    authStore.setAccessEstablishment(id)
  }

  useEffect(() => {
    establishmentStore.getEstablishments()
  }, [])

  function toggleDropdown(): void {
    setIsDropdownOpen(!isDropdownOpen)
  }
  return (
    <Dropdown direction="left" isOpen={isDropdownOpen} toggle={toggleDropdown}>
      <DropdownToggle
        data-toggle="dropdown"
        tag="button"
        className={classNames(className, 'dropdown-item notify-item')}
        aria-expanded={isDropdownOpen}
      >
        <i className={cx(`${icon} mr-1`, styles.iconStyle)}></i>
        <span>{label}</span>
        <i className={`ml-1 las la-angle-right`}></i>
      </DropdownToggle>
      <DropdownMenu className={styles.dropdownModule} positionFixed={true}>
        {establishmentOptions.map((option, index) => (
          <DropdownItem
            className={classNames(styles.dropdownItem, {
              [styles.dropdownItemActive]: option.value === authStore.establishment,
            })}
            toggle={false}
            key={index + option.value}
            onClick={() => handleChangeEst(option.value)}
          >
            {option.value === authStore.establishment ? (
              <i className={cx(`uil uil-check mr-1`, styles.iconCheck)}></i>
            ) : (
              <></>
            )}
            <span className={option.value === authStore.establishment ? '' : styles.unselected}>{option.label}</span>
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  )
}

export default SwitchEstablishmentDropdown
