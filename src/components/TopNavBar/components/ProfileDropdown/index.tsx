import { Fragment, useState } from 'react'
import classNames from 'classnames'
import cx from 'classnames'
import { useStore } from 'hooks/useStore'
import { Link } from 'react-router-dom'
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap'
import Icon, { IconGroupPrefix } from 'components/Icon'
import AuthStore from 'stores/authStore'
import SwitchEstablishmentDropdown from './components/SwitchEstablishmentDropdown'
import styles from './profileDropdown.module.scss'

interface IProfileMenuItem {
  label: string
  icon: string
  redirectTo: string
  handler?: (authStore: AuthStore) => void
}

interface IProfileDropdownProps {
  menuItems: IProfileMenuItem[]
  profilePic?: any
  username: string
  userTitle?: string
}

const ProfileDropdown = (props: IProfileDropdownProps) => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const [iconAngle, setIconAngle] = useState('angle-down')
  const profilePic = props.profilePic || null
  const { authStore } = useStore()

  function toggleDropdown(): void {
    if (iconAngle === 'angle-down') {
      setIconAngle('angle-up')
    } else {
      setIconAngle('angle-down')
    }
    setIsDropdownOpen(!isDropdownOpen)
  }

  return (
    <Dropdown isOpen={isDropdownOpen} className={styles.dropdown} toggle={toggleDropdown}>
      <DropdownToggle
        data-toggle="dropdown"
        tag="button"
        className={classNames('nav-link nav-user arrow-none mr-0', styles.dropdownToggle)}
        onClick={toggleDropdown}
        aria-expanded={isDropdownOpen}
      >
        <span className={classNames(styles.accountUserAvatar, 'account-user-avatar')}>
          <img src={profilePic} className="rounded-circle" alt="user" />
        </span>
        <span>
          <span className="account-user-name">{props.username}</span>
          <span className="account-position">{props.userTitle}</span>
        </span>
        <Icon group={IconGroupPrefix.LAICON} icon={iconAngle} className={styles.icon} />
      </DropdownToggle>
      <DropdownMenu
        right
        positionFixed={true}
        className={classNames(styles.dropdownMenu, 'dropdown-menu-animated topbar-dropdown-menu profile-dropdown')}
      >
        <div>
          {props.menuItems.map((item, i) => {
            return (
              <Fragment key={i + '-profile-menu'}>
                {item.label !== 'Switch establishment' ? (
                  <Link
                    to={item.redirectTo}
                    className={classNames('dropdown-item notify-item', styles.dropdownItem, {
                      [styles.logout]: item.label === 'Logout',
                    })}
                    onClick={() => item.handler && item.handler(authStore)}
                  >
                    <i className={cx(`${item.icon} mr-1`, styles.iconStyle)}></i>
                    <span>{item.label}</span>
                  </Link>
                ) : (
                  <SwitchEstablishmentDropdown icon={item.icon} label={item.label} className={styles.dropdownItem} />
                )}
              </Fragment>
            )
          })}
        </div>
      </DropdownMenu>
    </Dropdown>
  )
}

export default ProfileDropdown
