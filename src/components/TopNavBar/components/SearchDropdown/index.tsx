import { useState } from 'react'
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap'
import styles from './searchDropdown.module.scss'

const SearchDropdown = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)

  function toggleDropdown(): void {
    setIsDropdownOpen(!isDropdownOpen)
  }

  return (
    <Dropdown className={styles.dropdownWrapper} isOpen={isDropdownOpen} toggle={toggleDropdown}>
      <DropdownToggle
        data-toggle="dropdown"
        tag="a"
        className="nav-link dropdown-toggle arrow-none mr-0"
        onClick={toggleDropdown}
        aria-expanded={isDropdownOpen}
      >
        <i className="mdi mdi-magnify noti-icon"></i>
      </DropdownToggle>
      <DropdownMenu right className="dropdown-menu-animated topbar-dropdown-menu dropdown-lg">
        <form className="p-3">
          <input type="text" className="form-control" placeholder="Search ..." />
        </form>
      </DropdownMenu>
    </Dropdown>
  )
}

export default SearchDropdown
