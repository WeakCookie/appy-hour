import { useState } from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap'
import styles from './styles.module.scss'

interface ISearchResultItem {
  id: number
  title: string
  redirectTo: string
  icon: string
}

interface ITopbarSearchProps {
  items: ISearchResultItem[]
}

const TopbarSearch = (props: ITopbarSearchProps) => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false)
  const resultItems = props.items || []

  function toggleDropdown(): void {
    setIsDropdownOpen(!isDropdownOpen)
  }

  return (
    <Dropdown isOpen={isDropdownOpen} toggle={toggleDropdown} className="app-search d-none d-lg-block">
      <DropdownToggle tag="a" className="d-none"></DropdownToggle>
      <form className="position-relative">
        <div className="input-group">
          <input
            className="form-control dropdown-toggle"
            placeholder="Search..."
            id="top-search"
            onClick={toggleDropdown}
          />
          <span className="mdi mdi-magnify search-icon"></span>
          <div className="input-group-append">
            <button className="btn btn-primary" type="submit">
              Search
            </button>
          </div>
        </div>
        <DropdownMenu
          right
          className={classNames('dropdown-menu-animated topbar-dropdown-menu dropdown-lg', styles.wrapper)}
        >
          <div className="dropdown-header noti-title">
            <h5 className="text-overflow mb-2">
              Found <span className="text-danger">{resultItems.length}</span> results
            </h5>
          </div>

          {resultItems.map((item, i) => {
            return (
              <Link key={i} to={item.redirectTo} className={classNames('dropdown-item', 'notify-item')}>
                <i className={classNames(item.icon, 'font-16', 'mr-1')}></i>
                <span>{item.title}</span>
              </Link>
            )
          })}
        </DropdownMenu>
      </form>
    </Dropdown>
  )
}

export default TopbarSearch
