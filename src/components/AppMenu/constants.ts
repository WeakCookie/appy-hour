import routes from "routes";
import AuthStore from "stores/authStore";

interface SidebarItem {
  path: string;
  name: string;
  icon: string;
  header?: string;
  handler?: (authStore: AuthStore) => void;
}

const dashboardRoutes: SidebarItem = {
  path: routes.special.value,
  name: "Specials",
  icon: "las la-cocktail",
  header: "ESTABLISHMENT DASHBOARD",
};

const eventRoutes: SidebarItem = {
  path: routes.event.value,
  name: "Events",
  icon: "las la-calendar",
};
const analyticsRoutes: SidebarItem = {
  path: routes.analytics.value,
  name: "Analytics",
  icon: "las la-chart-bar",
};

const businessInfoRoutes: SidebarItem = {
  path: routes.businessInfo.value,
  name: "Business Info",
  icon: "las la-sticky-note",
};

const campaignRoutes: SidebarItem = {
  path: routes.campaign.value,
  name: "Campaign",
  icon: "las la-chalkboard",
  header: "SUPPLIER DASHBOARD",
};

// const preferredPartnersRoutes: SidebarItem = {
//   path: routes.preferredPartners.value,
//   name: 'Preferred Partners',
//   icon: 'las la-school',
// }

export const sidebarItems = [
  dashboardRoutes,
  eventRoutes,
  analyticsRoutes,
  businessInfoRoutes,
];

export const supplierSidebarItems = [campaignRoutes];
