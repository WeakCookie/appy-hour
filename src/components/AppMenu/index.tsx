import { Fragment } from "react";
import classNames from "classnames";
import { useStore } from "hooks/useStore";
import { observer } from "mobx-react";
import { RoleType } from "constants/enums/role";
import MenuItem from "./components/MenuItem";
import { sidebarItems, supplierSidebarItems } from "./constants";
import styles from "./appMenu.module.scss";

interface IAppMenuProps {
  isCondensed?: boolean;
}

const AppMenu = (props: IAppMenuProps) => {
  const { authStore } = useStore();
  const { role } = authStore;
  const { isCondensed } = props;
  const sidebarContent =
    role === RoleType.SUPPLIER ? supplierSidebarItems : sidebarItems;

  return (
    <>
      <div className={classNames("topbar-nav", styles.appMenu)}>
        <ul className="metismenu side-nav" id="menu-bar">
          {sidebarContent.map((item, index: number) => {
            return (
              <Fragment key={index.toString()}>
                <>
                  {item?.header && (
                    <li
                      className="side-nav-title side-nav-item"
                      key={index.toString() + "-element"}
                    >
                      {item.header}
                    </li>
                  )}

                  {/*
                  //* INFO: use this in case a menu item has children
                  {item.children ? (
                    <MenuItemWithChildren
                      item={item}
                      subMenuClassNames="side-nav-second-level"
                      activatedMenuItemIds={activatedKeys}
                      linkClassNames="side-nav-link"
                    />
                  ) : ( */}
                  <MenuItem
                    item={item}
                    className={classNames({
                      "mm-active": window.location.pathname.includes(item.path),
                    })}
                    isCondensed={isCondensed}
                    linkClassName="side-nav-link"
                  />
                </>
              </Fragment>
            );
          })}
        </ul>
      </div>
    </>
  );
};

export default observer(AppMenu);
