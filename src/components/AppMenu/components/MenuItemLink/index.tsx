import cx from 'classnames'
import { useStore } from 'hooks/useStore'
import { Link } from 'react-router-dom'
import AuthStore from 'stores/authStore'
import styles from './styles.module.scss'

interface IMenuItemLinkProps {
  item: {
    path: string
    icon: string
    badge?: {
      variant: string
      text: string
    }
    name: string
    handler?: (authStore: AuthStore) => void
  }
  className?: string
  isCondensed?: boolean
}

const MenuItemLink = (props: IMenuItemLinkProps) => {
  const { item, className, isCondensed } = props
  const { authStore } = useStore()
  function handleOnclick() {
    if (authStore && item?.handler) {
      item.handler(authStore)
    }
  }
  return (
    <Link
      to={item.path}
      className={cx('side-nav-link-ref', 'side-sub-nav-link', className, { [styles.activeLink]: isCondensed })}
      onClick={handleOnclick}
    >
      {item.icon && <i className={cx(item.icon, styles.icon)}></i>}
      {item.badge && <span className={`badge badge-${item.badge.variant} float-right`}>{item.badge.text}</span>}
      {!isCondensed && <span> {item.name} </span>}
    </Link>
  )
}

export default MenuItemLink
