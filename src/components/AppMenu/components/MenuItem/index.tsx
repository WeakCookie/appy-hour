import classNames from 'classnames'
import AuthStore from 'stores/authStore'
import MenuItemLink from '../MenuItemLink'

interface IMenuItemProps {
  item: {
    path: string
    icon: string
    badge?: {
      variant: string
      text: string
    }
    name: string
    handler?: (authStore: AuthStore) => void
  }
  className?: string
  linkClassName: string
  isCondensed?: boolean
}

const MenuItem = (props: IMenuItemProps) => {
  const { item, className, linkClassName, isCondensed } = props
  return (
    <li className={classNames('side-nav-item', className)}>
      <MenuItemLink isCondensed={isCondensed} item={item} className={linkClassName} />
    </li>
  )
}

export default MenuItem
