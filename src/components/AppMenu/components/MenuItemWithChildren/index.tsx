import { Fragment } from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import MenuItem from '../MenuItem'

interface IMenuItem {
  path: string
  icon: string
  badge: {
    variant: string
    text: string
  }
  name: string
  id: string
  children: IMenuItem[]
}

interface IMenuItemWithChildrenProps {
  item: IMenuItem
  linkClassNames: string
  subMenuClassNames: string
  activatedMenuItemIds: string[]
}

const MenuItemWithChildren = ({
  item,
  linkClassNames,
  subMenuClassNames,
  activatedMenuItemIds,
}: IMenuItemWithChildrenProps) => {
  const firstActivatedMenuItemIndex = 0
  return (
    <li
      className={classNames('side-nav-item', {
        'mm-active': activatedMenuItemIds.indexOf(item.id) >= firstActivatedMenuItemIndex,
        active: activatedMenuItemIds.indexOf(item.id) >= firstActivatedMenuItemIndex,
      })}
    >
      <Link
        to="#"
        className={classNames('has-arrow', 'side-sub-nav-link', linkClassNames)}
        aria-expanded={activatedMenuItemIds.indexOf(item.id) >= firstActivatedMenuItemIndex}
      >
        {item.icon && <i className={item.icon}></i>}
        {item.badge && <span className={`badge badge-${item.badge.variant} float-right`}>{item.badge.text}</span>}
        <span> {item.name} </span>
      </Link>

      <ul
        className={classNames(subMenuClassNames, 'mm-collapse', {
          'mm-collapsed mm-show': activatedMenuItemIds.indexOf(item.id) >= firstActivatedMenuItemIndex,
        })}
        aria-expanded={activatedMenuItemIds.indexOf(item.id) >= firstActivatedMenuItemIndex}
      >
        {item.children.map((child, i) => {
          return (
            <Fragment key={i}>
              {child.children ? (
                <MenuItemWithChildren
                  item={child}
                  linkClassNames=""
                  activatedMenuItemIds={activatedMenuItemIds}
                  subMenuClassNames="side-nav-third-level"
                />
              ) : (
                <MenuItem
                  item={child}
                  className={classNames({
                    'mm-active': activatedMenuItemIds.indexOf(child.id) >= firstActivatedMenuItemIndex,
                  })}
                  linkClassName=""
                />
              )}
            </Fragment>
          )
        })}
      </ul>
    </li>
  )
}

export default MenuItemWithChildren
