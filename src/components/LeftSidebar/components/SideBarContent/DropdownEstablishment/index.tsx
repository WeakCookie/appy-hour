import { useEffect, useState } from 'react'
import { useStore } from 'hooks/useStore'
import { observer } from 'mobx-react'
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
import { getValidArray } from 'utils/common'
import styles from './dropdownEstablishment.module.scss'

const DropdownEstablishment = () => {
  const { authStore, establishmentStore } = useStore()
  const { establishment } = authStore
  const { establishments = [] } = establishmentStore
  const establishmentOptions = getValidArray(establishments).map((establishment: any) => ({
    label: establishment.name,
    value: establishment.establishment_id,
  }))
  const currentEstablishment = getValidArray(establishments).find((est) => est.establishment_id === establishment)
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const toggle = () => setDropdownOpen((prevState) => !prevState)

  function handleChangeEst(id: number) {
    authStore.setAccessEstablishment(id)
  }

  useEffect(() => {
    establishmentStore.getEstablishments()
  }, [])

  return (
    <Dropdown className={styles.wrapper} isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle caret>{currentEstablishment?.name ?? ''}</DropdownToggle>
      <DropdownMenu>
        {establishmentOptions.map((option) => (
          <DropdownItem key={`${option.value}-${option.label}}`} onClick={() => handleChangeEst(option.value)}>
            {option.label}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  )
}

export default observer(DropdownEstablishment)
