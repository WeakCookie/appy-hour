import profileImg from 'Assets/images/Avatar/avatar.png'
import { Link } from 'react-router-dom'
import AppMenu from '../../../AppMenu'

interface ISideBarContentProps {
  hideUserProfile: boolean
  isCondensed?: boolean
}

const SideBarContent = (props: ISideBarContentProps) => {
  const { hideUserProfile, isCondensed } = props

  return (
    <>
      {!hideUserProfile && (
        <div className="leftbar-user">
          <Link to="/">
            <img src={profileImg} alt="" height="42" className="rounded-circle shadow-sm" />
            <span className="leftbar-user-name">Kieu Dang</span>
          </Link>
        </div>
      )}

      <AppMenu isCondensed={isCondensed} />
    </>
  )
}

export default SideBarContent
