import { useEffect } from 'react'
import logoSm from 'Assets/images/app_icon.png'
import logo from 'Assets/images/app_icon_1024.png'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import SimpleBar from 'simplebar-react'
import SideBarContent from './components/SideBarContent'
import 'simplebar/dist/simplebar.min.css'
import styles from './leftSidebar.module.scss'

interface ILeftSidebarProps {
  menuClickHandler?: unknown
  hideLogo?: boolean
  hideUserProfile?: boolean
  isCondensed: boolean
}

const LeftSidebar = (props: ILeftSidebarProps) => {
  const isCondensed = props.isCondensed || false
  const hideLogo = props.hideLogo || false
  const hideUserProfile = props.hideUserProfile || false
  let menuNodeRef: HTMLDivElement | null

  function handleOtherClick(event: any): void {
    if (menuNodeRef?.contains(event.target)) return
    // else hide the menubar
    if (document.body) {
      document.body.classList.remove('sidebar-enable')
    }
  }

  /**
   * Bind event
   */
  useEffect(() => {
    document?.addEventListener('mousedown', handleOtherClick, false)
    return () => {
      document?.removeEventListener('mousedown', handleOtherClick, false)
    }
  }, [])

  return (
    <>
      <div
        className={classNames(styles.leftSideMenu, 'left-side-menu')}
        ref={(node) => {
          menuNodeRef = node
        }}
      >
        {!hideLogo && (
          <>
            <Link to="/" className="logo text-center logo-light">
              <span className={'logo-lg'}>
                <span className={styles.logoLgWrapper}>
                  <img src={logo} alt="logo" height="24" />
                  <span className={styles.appName}>APPYHOUR</span>
                </span>
              </span>
              <span className="logo-sm">
                <img src={logoSm} alt="logo" height="16" />
              </span>
            </Link>
          </>
        )}

        {!isCondensed && (
          <SimpleBar className={styles.simpleNavbar} timeout={500} scrollbarMaxSize={320}>
            <SideBarContent hideUserProfile={hideUserProfile} />
          </SimpleBar>
        )}
        {isCondensed && <SideBarContent isCondensed={isCondensed} hideUserProfile={hideUserProfile} />}
      </div>
    </>
  )
}

export default LeftSidebar
