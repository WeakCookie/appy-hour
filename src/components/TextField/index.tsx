import cx from 'classnames'
import { Input, InputProps } from 'reactstrap'
import styles from './textField.module.scss'

interface TextFieldProps {
  type?: 'text' | 'email' | 'password' | 'textarea'
  error?: boolean
}

const TextField = ({ className, error = false, type = 'text', ...props }: InputProps & TextFieldProps) => {
  const classes = [styles.hfTextField]
  if (error) classes.push(styles.errorWrapper)
  return <Input type={type} className={cx(...classes, className)} {...props} />
}

export default TextField
