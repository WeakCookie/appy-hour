import cx from 'classnames'
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu as RSDropdownMenu,
  UncontrolledButtonDropdownProps,
} from 'reactstrap'
import styles from './dropdownMenu.module.scss'

interface DropdownMenuProps {
  caret?: boolean
  placeholder?: string
  color?: string
  animated?: boolean
}

const DropdownMenu = ({
  className,
  children,
  caret = false,
  placeholder = '',
  color = 'primary',
  animated = false,
  ...props
}: UncontrolledButtonDropdownProps & DropdownMenuProps) => {
  return (
    <UncontrolledButtonDropdown {...props} className={cx(styles.hfDropdownMenu, className)}>
      {placeholder ? (
        <DropdownToggle caret={caret} color={color}>
          {placeholder}
        </DropdownToggle>
      ) : null}
      {children ? (
        <RSDropdownMenu className={animated ? styles.dropdownMenuAnimated : undefined}>{children}</RSDropdownMenu>
      ) : null}
    </UncontrolledButtonDropdown>
  )
}

export default DropdownMenu
