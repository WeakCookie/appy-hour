import Button from 'components/Button'
import ModalDialog from 'components/ModalDialog'

interface IConfirmContinueModalProps {
  isOpen: boolean | undefined
  className?: string
  setOpen: (value: boolean) => void
  onAcceptLostButton: () => void
}

const ConfirmContinueModal = (props: IConfirmContinueModalProps) => {
  const { isOpen, className, setOpen, onAcceptLostButton } = props

  return (
    <ModalDialog
      title={'Warning Submit'}
      size="sm"
      toggle={() => setOpen(false)}
      isOpen={isOpen}
      className={className}
      footer={
        <>
          <Button color="secondary" type="button" onClick={() => setOpen(false)}>
            Back To Edit
          </Button>
          <Button color="primary" type="button" onClick={onAcceptLostButton}>
            Continue
          </Button>
        </>
      }
    >
      You haven't saved yet, so your changes will be lost. Are you sure you want to continue?
    </ModalDialog>
  )
}

export default ConfirmContinueModal
