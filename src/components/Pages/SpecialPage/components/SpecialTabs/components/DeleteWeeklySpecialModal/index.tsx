import { useState } from 'react'
import dayjs from 'dayjs'
import { useStore } from 'hooks/useStore'
import capitalize from 'lodash/capitalize'
import { observer } from 'mobx-react'
import { toast } from 'react-toastify'
import { Col, Row } from 'reactstrap'
import { deleteSpecial } from 'Api/special'
import Button from 'components/Button'
import ModalDialog from 'components/ModalDialog'
import { getDealImage } from 'components/Pages/AnalyticsPage/utils'
import { SpecialType } from 'components/Pages/SpecialPage/constants'
import { getPriceTitle } from 'components/Pages/SpecialPage/utils'
import StatusCell from 'components/Table/components/StatusCell'
import Title from 'components/Title'
import { IMAGE_SOURCE_URL } from 'constants/common'
import { CategoryType } from 'constants/enums/special'
import { WeekDayEnum } from '../CreateWeeklySpecialModal/constants'
import LabelValueLine from '../LabelValueLine'
import styles from './reviewSpecialModal.module.scss'

interface IReviewSpecialModal {
  isOpening: boolean
  toggleShow: () => void
}

const DeleteSpecialModal = (props: IReviewSpecialModal) => {
  const { isOpening = false, toggleShow } = props
  const { specialStore } = useStore()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const { formData } = specialStore
  const {
    description,
    price = 0,
    percentage = 0,
    priceType = 300,
    dealType = 200,
    type,
    startTime = new Date(),
    endTime = new Date(),
    days = [],
    editingSpecialId = 0,
  } = formData
  const displayPrice = getPriceTitle(+price, +percentage, priceType)

  async function handleSubmit(): Promise<void> {
    setIsSubmitting(true)
    try {
      await deleteSpecial(editingSpecialId)
      toast.success('Delete special successfully')
      specialStore.getTablesData()
    } catch (error) {
      toast.error('Delete special failed')
    } finally {
      setIsSubmitting(false)
      specialStore.setOpenReviewModal(false, SpecialType.WEEKLY, false)
    }
  }

  return (
    <ModalDialog
      title={`Delete Weekly Special`}
      toggle={toggleShow}
      isOpen={isOpening}
      className={styles.modal}
      footer={
        <>
          <Button
            className={styles.infoButton}
            color="secondary"
            onClick={() => specialStore.setOpenReviewModal(false, SpecialType.WEEKLY, false)}
          >
            Cancel
          </Button>
          <Button color="danger" type="submit" onClick={handleSubmit} disabled={isSubmitting}>
            Yes, Delete it
          </Button>
        </>
      }
    >
      <Row>
        <Col md={12}>
          <Title small>Are you sure you want to delete this special?</Title>
        </Col>
      </Row>
      <Row>
        <Col md={3}>
          <div className={styles.cardWrapper}>
            <img className={styles.image} src={`${IMAGE_SOURCE_URL}/${getDealImage(+dealType)}`} alt="logo" />
            <h5 className={styles.cardLabel}>{`${displayPrice}`}</h5>
          </div>
        </Col>
        <Col md={9}>
          <h4>{`${displayPrice} ${description}`}</h4>
          {type.includes(CategoryType.DINEIN) && (
            <StatusCell status={CategoryType.DINEIN}>{CategoryType.DINEIN}</StatusCell>
          )}
          {type.includes(CategoryType.TAKEOUT) && (
            <StatusCell className={styles.padding} status={CategoryType.TAKEOUT}>
              {CategoryType.TAKEOUT}
            </StatusCell>
          )}
          <LabelValueLine label={'Day(s)'} value={days.map((day: WeekDayEnum) => capitalize(day)).join(', ')} />
          <div className={styles.timeContainer}>
            <LabelValueLine label={'Start time'} value={`${dayjs(startTime).format('h:mm A')}  -`} />
            <LabelValueLine
              className={styles.endTime}
              label={'End time'}
              value={`${dayjs(endTime).format('h:mm A')}`}
            />
          </div>
        </Col>
      </Row>
    </ModalDialog>
  )
}

export default observer(DeleteSpecialModal)
