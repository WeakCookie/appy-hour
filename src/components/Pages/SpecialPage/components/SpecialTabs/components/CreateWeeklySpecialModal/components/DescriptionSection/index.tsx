import cx from 'classnames'
import { Col, Row } from 'reactstrap'
import Input from 'components/Input'
import Title from 'components/Title'
import styles from '../../createSpecialModal.module.scss'

interface IDescriptionProps {
  register: any
  errors: any
  isEditing: boolean
}

const DescriptionSection = ({ register, errors, isEditing }: IDescriptionProps) => {
  return (
    <Row>
      <Col md={12}>
        <Title isRequired small hasMarginTop>
          Description
        </Title>
        <Input
          name="description"
          className={cx({
            [styles.error]: !!errors['description'],
            [styles.disabledInput]: isEditing,
          })}
          placeholder="Details of Special"
          innerRef={register({ required: true, maxLength: 40 })}
        />
        <p className={styles.hintText}>Limit 40 characters</p>
      </Col>
    </Row>
  )
}

export default DescriptionSection
