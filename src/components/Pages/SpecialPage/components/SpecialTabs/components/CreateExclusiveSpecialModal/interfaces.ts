export interface ICreateSpecialModal {
  isOpening: boolean
  toggleShow: () => void
  handleReview: () => void
}
