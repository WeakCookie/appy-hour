import { WeekDayEnum } from '../../../SpecialTabs/components/CreateWeeklySpecialModal/constants'

export function getDaysOfWeek(day: number): WeekDayEnum {
  const dayOfWeek = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
  return dayOfWeek[day] as WeekDayEnum
}
