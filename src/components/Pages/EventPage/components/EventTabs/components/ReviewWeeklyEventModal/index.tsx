import dayjs from 'dayjs'
import { useStore } from 'hooks/useStore'
import get from 'lodash/get'
import { observer } from 'mobx-react'
import { Col, Row } from 'reactstrap'
import Button from 'components/Button'
import Icon from 'components/Icon'
import ModalDialog from 'components/ModalDialog'
import Title from 'components/Title'
import { getCapitalizeArray } from 'utils/common'
import SpecialList from '../../../SpecialList'
import LabelValueLine from '../LabelValueLine'
import styles from './reviewEventModal.module.scss'

interface IReviewEventModal {
  isOpening: boolean
  toggleShow: () => void
}

const ReviewEventModal = (props: IReviewEventModal) => {
  const { isOpening = false, toggleShow } = props
  const { eventStore } = useStore()
  const { reviewData } = eventStore
  const { title, details, start_time, end_time, event_id, weekDay } = reviewData

  return (
    <ModalDialog
      title={'Create new Weekly Event'}
      toggle={toggleShow}
      isOpen={isOpening}
      className={styles.modal}
      footer={
        <>
          <Button color="primary" onClick={toggleShow}>
            Done
          </Button>
        </>
      }
    >
      <Row>
        <Col md={12}>
          <Title small>Event created successfully!</Title>
        </Col>
      </Row>
      <Row>
        <div>
          <div className={styles.cardWrapper}>
            <Icon icon="calendar-check" className={styles.iconStyle} />
          </div>
        </div>
        <Col md={10}>
          <h4>{title}</h4>
          <Title small isBlock>
            {details}
          </Title>
          <LabelValueLine label={'Day(s)'} value={` ${getCapitalizeArray(weekDay).join(', ')}`} />
          <LabelValueLine label={'Start time'} value={dayjs(start_time).format('h:mm A')} inline />
          <LabelValueLine label={'- End time'} value={dayjs(end_time).format('h:mm A')} inline />
        </Col>
      </Row>
      <SpecialList eventId={event_id} specials={get(reviewData, 'specials', [])} key="review-one-time-event-specials" />
    </ModalDialog>
  )
}

export default observer(ReviewEventModal)
