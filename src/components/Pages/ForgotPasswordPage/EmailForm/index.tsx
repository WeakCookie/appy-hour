import { FormGroup, Label } from 'reactstrap'
import ButtonWithIcon from 'components/ButtonWithIcon'
import Input from 'components/InputGroup/Input'
import { RoleType } from 'constants/enums/role'
import RoleOption from '../../LoginPage/components/RoleOption'
import styles from './emailForm.module.scss'

interface EmailFormProps {
  email: string
  setEmail: (email: string) => void
  role: string
  setRole: (role: RoleType) => void
  isEmailInputted: boolean
  setIsEmailInputted: (isEmailInputted: boolean) => void
}

const EmailForm = (props: EmailFormProps) => {
  const { email, setEmail, role, setRole, setIsEmailInputted } = props

  const handleContinueButtonClick = () => {
    if (email) {
      setIsEmailInputted(true)
    }
  }

  return (
    <>
      <FormGroup>
        <Label className={styles.label}>Select Your Role</Label>
        <RoleOption role={role} setRole={setRole} />
      </FormGroup>
      <FormGroup>
        <Label className={styles.label}>Your Email</Label>
        <Input type="email" placeholder="Ex: john@doe.com" value={email} setValue={setEmail} />
      </FormGroup>
      <ButtonWithIcon content="Continue" className={styles.continueButton} onClick={handleContinueButtonClick} />
    </>
  )
}

export default EmailForm
