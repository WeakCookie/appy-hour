import { ITableHeader } from "types/table";

export enum EViewSwitcher {
  GRID_VIEW = "GRID VIEW",
  TABLE_VIEW = "TABLE VIEW",
}

export enum ETableHeaderType {
  TITLE = "title",
  STATUS = "status",
  DATE_TIME_GROUP = "dateTimeGroup",
  ACTIONS = "actions",
}

export enum EStatusType {
  PENDING_ACTIVATION = "Pending activation",
  PENDING_REVIEW = "Pending review",
}

export function getHeaderList() {
  const headers: ITableHeader[] = [
    {
      Header: "Title",
      accessor: "title",
    },
    {
      Header: "Label",
      accessor: "label",
    },
    {
      Header: "Status",
      accessor: "status",
    },
    {
      Header: "Impressions / Interactions",
      accessor: "dateTimeGroup",
    },
    {
      Header: " ",
      accessor: "actions",
    },
  ];

  return headers;
}

export interface ICampaignData {
  title: string;
  label: string;
  status: string;
  dateTimeGroup: {
    lastWeek: {
      impressions: { value: number; rate: number };
      interactions: { value: number; rate: number };
    };
    thisWeek: {
      impressions: { value: number; rate: number };
      interactions: { value: number; rate: number };
    };
    allTime: number;
  };
}

export const mockTableData: ICampaignData[] = [
  {
    title: "Have a nice weekend",
    label: "Another day of sun",
    status: "Pending activation",
    dateTimeGroup: {
      lastWeek: {
        impressions: { value: 129, rate: 25 },
        interactions: { value: 129, rate: -19 },
      },
      thisWeek: {
        impressions: { value: 129, rate: 25 },
        interactions: { value: 129, rate: -19 },
      },
      allTime: 129,
    },
  },
  {
    title: "Have a nice weekend",
    label: "Another day of sun",
    status: "Pending review",
    dateTimeGroup: {
      lastWeek: {
        impressions: { value: 129, rate: 25 },
        interactions: { value: 129, rate: -19 },
      },
      thisWeek: {
        impressions: { value: 129, rate: 25 },
        interactions: { value: 129, rate: -19 },
      },
      allTime: 129,
    },
  },
];
