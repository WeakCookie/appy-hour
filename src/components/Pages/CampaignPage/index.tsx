import { useState } from "react";
import Button from "components/Button";
import Icon, { IconGroupPrefix } from "components/Icon";
import Input from "components/InputGroup/Input";
import GridView from "./components/GridView";
import Table from "./components/TableView";
import ViewSwitcher from "./components/ViewSwitcher";
import { EViewSwitcher, getHeaderList, mockTableData } from "./constant";
import styles from "./campaignPage.module.scss";

const CampaignPage = () => {
  const [viewType, setViewType] = useState<EViewSwitcher>(
    EViewSwitcher.TABLE_VIEW
  );

  return (
    <div className={styles.container}>
      <div className={styles.filterBar}>
        <div className={styles.filterGroup}>
          <Input
            className={styles.inputSearch}
            iconName="search"
            iconGroup={IconGroupPrefix.LAICON}
            addonType="append"
            addonClassname={styles.inputSearchAddon}
          />
          <Button className={styles.filterButton}>
            <Icon group={IconGroupPrefix.LAICON} icon="filter" />
            {` `}Filter
          </Button>
          <ViewSwitcher viewType={viewType} setViewType={setViewType} />
        </div>
        <Button className={styles.createButton}>
          <Icon icon="plus" group={IconGroupPrefix.LAICON} />
          {` `}Create New
        </Button>
      </div>
      {viewType === EViewSwitcher.TABLE_VIEW ? (
        <div className={styles.tableViewWrapper}>
          <Table
            headerList={getHeaderList()}
            tableData={mockTableData}
            hasNoSort={true}
            pagination={{
              includePagination: true,
              tableLength: 100,
              pageIndex: 1,
              gotoPage: () => {},
            }}
          />
        </div>
      ) : (
        <GridView
          data={mockTableData}
          pagination={{
            includePagination: true,
            tableLength: 100,
            pageIndex: 1,
            gotoPage: () => {},
          }}
        />
      )}
    </div>
  );
};

export default CampaignPage;
