import cx from "classnames";
import { Button } from "reactstrap";
import Icon, { IconGroupPrefix } from "components/Icon";
import { EStatusType, ICampaignData } from "../../../../constant";
import styles from "./styles.module.scss";

interface ICampaignCardProps {
  item: ICampaignData;
}

const CampaignCard = (props: ICampaignCardProps) => {
  const { item } = props;
  const { title, label, status, dateTimeGroup } = item;
  const { thisWeek, lastWeek } = dateTimeGroup;
  return (
    <div className={styles.campaignContainer}>
      <div className={styles.imageWrapper}>
        {status === EStatusType.PENDING_ACTIVATION && (
          <div className={styles.centerTexts}>
            <div>Lovely night</div>
            <div>The lights are tuning on</div>
          </div>
        )}
        {status === EStatusType.PENDING_REVIEW && (
          <div className={styles.leftBottomTexts}>
            <div>{title}</div>
            <div>{label}</div>
          </div>
        )}
      </div>
      <div className={styles.infoContainer}>
        <div className={styles.impressionGroup}>
          <div className={styles.infoTitle}>
            <Icon icon="eye" group={IconGroupPrefix.LAICON} />
            <span>Impression</span>
          </div>
          <div className={styles.infoContent}>
            <div>
              <div>05/07 - 05/13</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>
                  {lastWeek.impressions.value}
                </span>
                <span
                  className={cx(styles.rate, {
                    [styles.increment]: lastWeek.impressions.rate >= 0,
                    [styles.decrement]: lastWeek.impressions.rate < 0,
                  })}
                >
                  {lastWeek.impressions.rate >= 0 && "+"}
                  {lastWeek.impressions.rate}%
                </span>
              </div>
            </div>
            <div>
              <div>05/14 - 05/21</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>
                  {thisWeek.impressions.value}
                </span>
                <span
                  className={cx(styles.rate, {
                    [styles.increment]: thisWeek.impressions.rate >= 0,
                    [styles.decrement]: thisWeek.impressions.rate < 0,
                  })}
                >
                  {thisWeek.impressions.rate >= 0 && "+"}
                  {thisWeek.impressions.rate}%
                </span>
              </div>
            </div>
            <div>
              <div>All time</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>{dateTimeGroup.allTime}</span>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.interactionGroup}>
          <div className={styles.infoTitle}>
            <Icon icon="hand-point-up" group={IconGroupPrefix.LAICON} />
            <span>Interaction</span>
          </div>
          <div className={styles.infoContent}>
            <div>
              <div>05/07 - 05/13</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>
                  {lastWeek.interactions.value}
                </span>
                <span
                  className={cx(styles.rate, {
                    [styles.increment]: lastWeek.interactions.rate >= 0,
                    [styles.decrement]: lastWeek.interactions.rate < 0,
                  })}
                >
                  {lastWeek.interactions.rate >= 0 && "+"}
                  {lastWeek.interactions.rate}%
                </span>
              </div>
            </div>
            <div>
              <div>05/14 - 05/21</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>
                  {thisWeek.interactions.value}
                </span>
                <span
                  className={cx(styles.rate, {
                    [styles.increment]: thisWeek.interactions.rate >= 0,
                    [styles.decrement]: thisWeek.interactions.rate < 0,
                  })}
                >
                  {thisWeek.interactions.rate >= 0 && "+"}
                  {thisWeek.interactions.rate}%
                </span>
              </div>
            </div>
            <div>
              <div>All time</div>
              <div className={styles.valueRateGroup}>
                <span className={styles.value}>{dateTimeGroup.allTime}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.actionBar}>
        <div
          className={cx(styles.status, {
            [styles.statusActivation]:
              status === EStatusType.PENDING_ACTIVATION,
            [styles.statusReview]: status === EStatusType.PENDING_REVIEW,
          })}
        >
          {status}
        </div>
        <div className={styles.actionButtonGroup}>
          <div className={styles.viewDetail}>
            <Icon icon="file-alt" group={IconGroupPrefix.LAICON} /> View Detail
          </div>
          {status === EStatusType.PENDING_ACTIVATION ? (
            <Button className={cx(styles.actionButton, styles.activateButton)}>
              <Icon icon="check-circle" group={IconGroupPrefix.LAICON} />{" "}
              Activate
            </Button>
          ) : (
            <Button className={cx(styles.actionButton, styles.editButton)}>
              <Icon icon="edit" group={IconGroupPrefix.LAICON} /> Edit
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

export default CampaignCard;
