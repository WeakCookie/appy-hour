import { IPagination } from "components/Table";
import Pagination from "components/Table/components/Pagination";
import { ICampaignData } from "../../constant";
import CampaignCard from "./components/CampaignCard";
import styles from "./styles.module.scss";

interface IGridViewProps {
  pagination?: IPagination;
  data: ICampaignData[];
}

const GridView = (props: IGridViewProps) => {
  return (
    <>
      <div className={styles.gridViewWrapper}>
        {props.data.map((campaign: ICampaignData, index: number) => {
          return <CampaignCard key={`campaign-${index}`} item={campaign} />;
        })}
      </div>
      {props.pagination && <Pagination pagination={props.pagination} />}
    </>
  );
};

export default GridView;
