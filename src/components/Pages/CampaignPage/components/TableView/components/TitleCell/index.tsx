import styles from "./styles.module.scss";

interface ITitleCellProps {
  value: any;
}

const TitleCell = (props: ITitleCellProps) => {
  return (
    <td>
      <div className={styles.cellWrapper}>{props.value}</div>
    </td>
  );
};

export default TitleCell;
