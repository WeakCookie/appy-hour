import cx from "classnames";
import styles from "./styles.module.scss";

interface IDateCellProps {
  value: any;
}

const DateCell = (props: IDateCellProps) => {
  const { value } = props;
  return (
    <td>
      <div className={styles.cellWrapper}>
        <div className={styles.line}>
          <div className={styles.date}>
            <span className={styles.value}>
              {value.lastWeek.impressions.value}
            </span>
            <span
              className={cx(styles.rate, {
                [styles.increment]: value.lastWeek.impressions.rate >= 0,
                [styles.decrement]: value.lastWeek.impressions.rate < 0,
              })}
            >
              {value.lastWeek.impressions.rate >= 0 && "+"}
              {value.lastWeek.impressions.rate}%
            </span>
          </div>
          <div className={styles.date}>
            <span className={styles.value}>
              {value.thisWeek.impressions.value}
            </span>
            <span
              className={cx(styles.rate, {
                [styles.increment]: value.thisWeek.impressions.rate >= 0,
                [styles.decrement]: value.thisWeek.impressions.rate < 0,
              })}
            >
              {value.thisWeek.impressions.rate >= 0 && "+"}
              {value.thisWeek.impressions.rate}%
            </span>
          </div>
          <div>
            <span className={styles.value}>{value.allTime}</span>
          </div>
        </div>
        <hr />
        <div className={styles.line}>
          <div className={styles.date}>
            <span className={styles.value}>
              {value.lastWeek.interactions.value}
            </span>
            <span
              className={cx(styles.rate, {
                [styles.increment]: value.lastWeek.interactions.rate >= 0,
                [styles.decrement]: value.lastWeek.interactions.rate < 0,
              })}
            >
              {value.lastWeek.interactions.rate >= 0 && "+"}
              {value.lastWeek.interactions.rate}%
            </span>
          </div>
          <div className={styles.date}>
            <span className={styles.value}>
              {value.thisWeek.interactions.value}
            </span>
            <span
              className={cx(styles.rate, {
                [styles.increment]: value.thisWeek.interactions.rate >= 0,
                [styles.decrement]: value.thisWeek.interactions.rate < 0,
              })}
            >
              {value.thisWeek.interactions.rate >= 0 && "+"}
              {value.thisWeek.interactions.rate}%
            </span>
          </div>
          <div>
            <span className={styles.value}>{value.allTime}</span>
          </div>
        </div>
      </div>
    </td>
  );
};

export default DateCell;
