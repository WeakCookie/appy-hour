import Icon, { IconGroupPrefix } from "components/Icon";
import styles from "./styles.module.scss";

const ActionCell = () => {
  return (
    <td>
      <div className={styles.cellWrapper}>
        <Icon
          className={styles.icon}
          icon="file-alt"
          group={IconGroupPrefix.LAICON}
        />
        <Icon
          className={styles.icon}
          icon="check-circle"
          group={IconGroupPrefix.LAICON}
        />
      </div>
    </td>
  );
};

export default ActionCell;
